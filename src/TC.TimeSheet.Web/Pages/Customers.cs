using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.EntityFrameworkCore;
using Microsoft.JSInterop;
using TC.TimeSheet.Web.Data.EF;
using TC.TimeSheet.Web.Models;
using TC.TimeSheet.Web.Pages.Components;

namespace TC.TimeSheet.Web.Pages
{
    public class CustomerComponent : ComponentBase
    {
        [Inject] 
        protected IJSRuntime JsRuntime { get; set; }

        [Inject]
        protected TimesheetEntities Entities { get; set; }
        
        protected CustomerItemRemoveDialogComponent _ctrlCustomerItemRemoveDialog;
        
        protected readonly List<CustomerReadModel> _customers = new List<CustomerReadModel>();

        protected override async void OnInitialized()
        {
            await JsRuntime.InvokeAsync<object>("initDateTimeRangePicker");

            await LoadCustomers();
        }

        private async Task LoadCustomers()
        {
            _customers.Clear();
            var customers = await Entities.Customers.ToListAsync();

            customers.ForEach(c =>
            {
                var model = new CustomerReadModel
                {
                    Id = c.Id.ToString(),
                    Name = c.Name,
                    AmountPerHour = c.AmountPerHour
                };

                _customers.Add(model);
            });

            StateHasChanged();
        }

        protected async void OnCustomerCreated(string s)
        {
            await LoadCustomers();
        }
        
        internal async Task RemoveCustomerItem(string customerId)
        {
            await _ctrlCustomerItemRemoveDialog.Show(customerId);
        }
        
        internal async void OnCustomerItemRemoved(string id)
        {
            _customers.Remove(_customers.First(c => c.Id == id));
        }
    }
}