using System;
using System.Globalization;
using TC.TimeSheet.Web.Extensions;

namespace TC.TimeSheet.Web.Models
{
    public class YearMonthModel
    {
        public YearMonthModel(DateTime date)
        {
            Month = date.Month.ToString("00");
            Year = date.Year.ToString();
            DisplayText = $"{DateTimeFormatInfo.CurrentInfo.GetMonthName(date.Month).FirstCharToUpper()} {date.Year}";
        }

        public string Id => $"{Year}{Month}";

        public string Year { get; }
        public string Month { get; }

        public string DisplayText { get; }
    }
}