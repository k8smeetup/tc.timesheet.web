using System.Collections.Generic;
using System.Linq;

namespace TC.TimeSheet.Web.Models
{
    public class TimesheetReportModel
    {
        public TimesheetReportModel()
        {
            ReportModelItems = new List<TimesheetReportModelItem>();
        }

        public List<TimesheetReportModelItem> ReportModelItems { get; set; }

        public decimal TotalAmount
        {
            get { return ReportModelItems.Sum(r => r.Amount); }
        }

    }
}