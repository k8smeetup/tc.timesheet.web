using System;

namespace TC.TimeSheet.Web.Models
{
    public class TimesheetCreateModel
    {
        public string Date { get; set; }
        public string CustomerId { get; set; }
        public string Hours { get; set; }
        public bool Paid { get; set; }
    }
}