namespace TC.TimeSheet.Web.Models
{
    public class TimesheetReportModelItem
    {
        public string Date { get; set; }

        public string Hours { get; set; }
        
        public decimal Amount { get; set; }
    }
}