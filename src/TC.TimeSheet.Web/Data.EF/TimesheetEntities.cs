using System;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Microsoft.Extensions.Options;
using TC.TimeSheet.Web.Data.Entities;

namespace TC.TimeSheet.Web.Data.EF
{
    public class TimesheetEntities : DbContext
    {
        public static readonly ILoggerFactory ConsoleLogger = LoggerFactory.Create(builder =>
        {
            builder.AddConsole();
        });
        
        public DbSet<Customer> Customers { get; set; }
        public DbSet<TimesheetItem> TimesheetItems { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
#if RELEASE
            optionsBuilder.UseSqlite($@"Data Source=timesheet.db");
#else
            optionsBuilder.UseLoggerFactory(ConsoleLogger);
            optionsBuilder.UseSqlite(@"Data Source=timesheet.db");
#endif
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>(c => c.HasQueryFilter(e => !e.Deleted));
            
            base.OnModelCreating(modelBuilder);
        }
    }
}