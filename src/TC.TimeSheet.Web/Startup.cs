using System.Globalization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.StaticFiles.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TC.TimeSheet.Web.Data.EF;
using TC.TimeSheet.Web.Shared;

namespace TC.TimeSheet.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddEntityFrameworkSqlite();
            
            services.AddDbContext<TimesheetEntities>(t => t.EnableSensitiveDataLogging());
            
            services.AddScoped<TimesheetState>();

            services.AddServerSideBlazor().AddCircuitOptions(o => { o.DetailedErrors = true; });
        }
        
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var entities = new TimesheetEntities())
            {
                entities.Database.Migrate();
            }

            var cultureInfo = new CultureInfo("it-IT");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.Use(
                next =>
                {
                    return async context =>
                    {
                        context.Response.OnStarting(
                            () =>
                            {
                                context.Response.Headers.Add("Cache-Control",
                                    "no-cache, no-store, must-revalidate"); // HTTP 1.1.
                                context.Response.Headers.Add("Pragma", "no-cache"); // HTTP 1.0.
                                context.Response.Headers.Add("Expires", "0"); //
                                return Task.CompletedTask;
                            });

                        await next(context);
                    };
                });

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}